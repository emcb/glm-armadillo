#pragma once
#include "baseFamily.h"
#include <boost/math/distributions/poisson.hpp>

template<class linkType>
 class poissonFamily:public baseFamily<linkType>{
public:
	mutable glmColF tdpois;
	
	poissonFamily():baseFamily<linkType>("poisson"){
	}
	
	void dpois(const glmMatF & x,const glmMatF & lambda,bool log_=false) const{
		//CHECK CONSISTENCY!!!
		glmMatF::const_iterator l=lambda.begin();
		glmMatF::iterator r=tdpois.begin();
		if(log_){
			for(const auto  & v:x){
				using boost::math::poisson;
				*r++=log(pdf(poisson(*l++),v));
			}
		}else{
			for(const auto  & v:x){
				using boost::math::poisson;
				*r++=pdf(poisson(*l++),v);
			}
		}
	}
	
	// variance: function: the variance as a function of the mean.
	inline glmColF variance(const glmColF & mu) const{
		return mu;
	}
	
	// dev.resids: function giving the deviance for each observation as a
	//           function of ‘(y, mu, wt)’, used by the ‘residuals’ method
	//           when computing deviance residuals.
	inline glmColF dev_resids(const glmColF & y,const glmColF & mu, const glmColF & wt) const{
		glmColF r = mu % wt;
		glmColA p = find(y > 0);
		r(p) = (wt(p) % (y(p) % log(y(p)/mu(p)) - (y(p) - mu(p))));
		return r * 2 ;
	}
	
	//      aic: function giving the AIC value if appropriate (but ‘NA’ for
	//           the quasi- families).  More precisely, this function returns
	//           -2 ll + 2 s, where ll is the log-likelihood and s is the
	//           number of estimated scale parameters.  Note that the penalty
	//           term for the location parameters (typically the “regression
	//           coefficients”) is added elsewhere, e.g., in ‘glm.fit()’, or
	//           ‘AIC()’, see the AIC example in ‘glm’.  See ‘logLik’ for the
	//           assumptions made about the dispersion parameter.
	inline glmFType aic(const glmColF & y,const glmColI & n,const glmColF & mu,const glmColF & wt, glmFType dev) const{
		dpois(y, mu, true);
		return -2 * sum(tdpois % wt);
	}

	// initialize: expression.  This needs to set up whatever data objects are
	//           needed for the family as well as ‘n’ (needed for AIC in the
	//           binomial family) and ‘mustart’ (see ‘glm’).
	inline void initialize(const glmColF & y , const glmColF & wt, const glmColF & start, const glmColF & etastart, glmColI & n, glmColF & mustart) const{
		if (arma::any(y < 0)) {
			throw glmException("negative values not allowed for the 'Poisson' family");
		}
		n = glmColI(y.n_rows,arma::fill::ones);
		if(mustart.n_rows==0){
			mustart=y+0.1;
		}
		tdpois=glmColF(y.n_rows);
	}

	//  validmu: logical function.  Returns ‘TRUE’ if a mean vector ‘mu’ is
	//           within the domain of ‘variance’.
	inline bool validmu(const glmColF & mu) const{
		return !mu.has_inf() && arma::all(mu > 0);
	}
	
	// valideta: logical function.  Returns ‘TRUE’ if a linear predictor ‘eta’
	//           is within the domain of ‘linkinv’.
	inline bool valideta(const glmColF & eta) const{
		return true;
	}
	
	inline std::pair<glmFType,bool>  dispersion(const glmColF & weights, const glmColF & residuals,glmIType df_residuals){
		return std::make_pair(1.0,false);
	}

	// simulate: (optional) function ‘simulate(object, nsim)’ to be called by
	//           the ‘"lm"’ method of ‘simulate’.  It will normally return a
	//           matrix with ‘nsim’ columns and one row for each fitted value,
	//           but it can also return a list of length ‘nsim’. Clearly this
	//           will be missing for ‘quasi-’ families.
};
