#pragma once
#include "baseFamily.h"
#include <boost/math/special_functions/digamma.hpp>
#include <boost/math/special_functions/trigamma.hpp>

template<class linkType>
class negativeBinomialFamily:public baseFamily<linkType>{
public:
	mutable glmFType i_theta;
	glmColF i_miny;
	negativeBinomialFamily():baseFamily<linkType>("negativeBinomial"){
	}
	
// 	negativeBinomialFamily(glmFType theta,const glmColF & y):baseFamily<linkType>("negativeBinomial"),i_miny(),i_theta(theta){
// 	};
	
// 	glmFType & tehta(){
// 		return i_theta;
// 	}
	
	
	// variance: function: the variance as a function of the mean.
	inline glmColF variance(const glmColF & mu) const{
// 		Col<double> v=mu + square(mu)/theta;
// 		cout << "RANGE di varianve " << min(v) << "\t" << max(v) << endl;
		return mu + square(mu)/i_theta;
	}
	
	// dev.resids: function giving the deviance for each observation as a
	//           function of ‘(y, mu, wt)’, used by the ‘residuals’ method
	//           when computing deviance residuals.
	inline glmColF  dev_resids(const glmColF & y,const glmColF  & mu, const glmColF  & wt) const{
// 		cout << "RANGE di mu: " << min(mu) << "\t" << max(mu) << endl;
// 		cout << "RANGE di y+theta: " << min(y+theta) << "\t" << max(y+theta) << endl;
// 		cout << "RANGE di mu+theta: " << min(mu+theta) << "\t" << max(mu+theta) << endl;
// 		cout << "RANGE di arma::max(miny, y): " << min(arma::max(miny, y)) << "\t" << max(arma::max(miny, y)) << endl;
		return 2 * wt % (y % arma::log(arma::max(glmColF(y.n_rows,arma::fill::ones), y)/mu)  - (y + i_theta) % arma::log((y + i_theta)/(mu + i_theta)));
	}
	
	//      aic: function giving the AIC value if appropriate (but ‘NA’ for
	//           the quasi- families).  More precisely, this function returns
	//           -2 ll + 2 s, where ll is the log-likelihood and s is the
	//           number of estimated scale parameters.  Note that the penalty
	//           term for the location parameters (typically the “regression
	//           coefficients”) is added elsewhere, e.g., in ‘glm.fit()’, or
	//           ‘AIC()’, see the AIC example in ‘glm’.  See ‘logLik’ for the
	//           assumptions made about the dispersion parameter.
	inline double aic(const glmColF & y,const glmColI & n,const glmColF & mu,const glmColF & wt, glmFType dev) const{
		glmColF yth=(y + i_theta);
		glmColF muth=(mu + i_theta);
		return 2 * arma::sum( ( yth % arma::log(muth) - y % arma::log(mu) + arma::lgamma(y + 1) - i_theta * std::log(i_theta) + std::lgamma(i_theta) - arma::lgamma(yth) ) % wt);
	}

	// initialize: expression.  This needs to set up whatever data objects are
	//           needed for the family as well as ‘n’ (needed for AIC in the
	//           binomial family) and ‘mustart’ (see ‘glm’).
	inline void initialize(const glmColF & y ,const glmColF & wt, const glmColF & start, const glmColF & etastart, glmColI & n, glmColF & mustart) const{
		if (arma::any(y < 0)) {
			throw glmException("negative values not allowed for the 'Negative binomial' family");
		}
		n = glmColI(y.n_rows,arma::fill::ones);
		if(mustart.n_rows==0){
			mustart=y;
			mustart(arma::find(y==0)).fill(1.0 / 6.0);
		}
	}

	//  validmu: logical function.  Returns ‘TRUE’ if a mean vector ‘mu’ is
	//           within the domain of ‘variance’.
	inline bool validmu(const glmColF & mu) const{
		return arma::all(mu > 0);
	}
	
	// valideta: logical function.  Returns ‘TRUE’ if a linear predictor ‘eta’
	//           is within the domain of ‘linkinv’.
	inline bool valideta(const glmColF  & eta) const{
		return true;
	}

	inline std::pair<double,bool>  dispersion(const glmColF & weights, const glmColF & residuals,glmIType df_residuals){
		return std::make_pair(1.0,false);
	}
	
	// simulate: (optional) function ‘simulate(object, nsim)’ to be called by
	//           the ‘"lm"’ method of ‘simulate’.  It will normally return a
	//           matrix with ‘nsim’ columns and one row for each fitted value,
	//           but it can also return a list of length ‘nsim’. Clearly this
	//           will be missing for ‘quasi-’ families.
};

