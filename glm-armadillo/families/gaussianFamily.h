#pragma once
#include "baseFamily.h"

template<class linkType>
class gaussianFamily:public baseFamily<linkType>{
public:
	gaussianFamily():baseFamily<linkType>("gaussian"){
	}
	
	// variance: function: the variance as a function of the mean.
	inline glmColF variance(const glmColF & mu) const{
		return glmColF(mu.n_rows,arma::fill::ones);
	}
	
	// dev.resids: function giving the deviance for each observation as a
	//           function of ‘(y, mu, wt)’, used by the ‘residuals’ method
	//           when computing deviance residuals.
	inline glmColF dev_resids(const glmColF & y,const glmColF & mu, const glmColF & wt) const{
		return wt % arma::square(y - mu);
	}
	
	//      aic: function giving the AIC value if appropriate (but ‘NA’ for
	//           the quasi- families).  More precisely, this function returns
	//           -2 ll + 2 s, where ll is the log-likelihood and s is the
	//           number of estimated scale parameters.  Note that the penalty
	//           term for the location parameters (typically the “regression
	//           coefficients”) is added elsewhere, e.g., in ‘glm.fit()’, or
	//           ‘AIC()’, see the AIC example in ‘glm’.  See ‘logLik’ for the
	//           assumptions made about the dispersion parameter.
	inline glmFType aic(const glmColF & y,const glmColI & n,const glmColF & mu,const glmColF & wt, glmFType dev) const{
		return (y.n_rows * (log(dev/y.n_rows * 2 * arma::datum::pi) + 1)) + 2 - arma::sum(arma::log(wt));
	}

	// initialize: expression.  This needs to set up whatever data objects are
	//           needed for the family as well as ‘n’ (needed for AIC in the
	//           binomial family) and ‘mustart’ (see ‘glm’).
	inline void initialize(const glmColF & y , const glmColF & wt, const glmColF & start, const glmColF & etastart,glmColI & n,glmColF & mustart) const{
// 		if ( (etastart.n_elem==0) && (start.n_elem==0) && (mustart.n_elem==0) && ((glmFamily<linkType>::linkObject.getName() == "inverse" && arma::any(y == 0)) || (glmFamily<linkType>::linkObject.getName()=="log" && arma::any(y <= 0)))){ 
		if ( (etastart.n_elem==0) && (start.n_elem==0) && (mustart.n_elem==0) && ((baseFamily<linkType>::getLinkName() == "inverse" && arma::any(y == 0)) || (baseFamily<linkType>::getLinkName()=="log" && arma::any(y <= 0)))){ 			
			throw glmException("cannot find valid starting values: please specify some");
		}
		n=glmColI(y.n_rows,arma::fill::ones);
		if(mustart.n_rows==0){
			mustart=y;
		}
	}

	//  validmu: logical function.  Returns ‘TRUE’ if a mean vector ‘mu’ is
	//           within the domain of ‘variance’.
	inline bool validmu(const glmColF & mu) const{
		return true;
	}
	
	// valideta: logical function.  Returns ‘TRUE’ if a linear predictor ‘eta’
	//           is within the domain of ‘linkinv’.
	inline bool valideta(const glmColF  & eta) const{
		return true;
	}

	// dispersion: Returns a pair with dispersion value and TRUE if dispersion is estimated from the data or FLASE otherwise
	inline std::pair<glmFType,bool> dispersion(const glmColF & weights, const glmColF & residuals,glmIType df_residuals){
		glmFType disp=arma::datum::nan;
		if(df_residuals>0){
			disp=arma::sum(weights % arma::square(residuals))/df_residuals;
		}
		return std::make_pair(disp,true);
	}

	// simulate: (optional) function ‘simulate(object, nsim)’ to be called by
	//           the ‘"lm"’ method of ‘simulate’.  It will normally return a
	//           matrix with ‘nsim’ columns and one row for each fitted value,
	//           but it can also return a list of length ‘nsim’. Clearly this
	//           will be missing for ‘quasi-’ families.
};
