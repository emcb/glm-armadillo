#pragma once
#include "../glmBase.h"

template<class  linkType>
class baseFamily:public linkType{
protected:
	std::string name;
public:
	//family: character: the family name.
	
	baseFamily(const std::string & name_):linkType(),name(name_){
	}
	
	inline const std::string & getFamilyName() const{
		return name;
	}

	
// 	//  linkfun: function: the link.
// 	inline glmColF linkfun(const glmColF & mu) const{
// 		return linkObject.linkfun(mu);
// 	}
// 	
// 	//  linkinv: function: the inverse of the link function.
// 	inline glmColF linkinv(const glmColF & eta)  const{
// 		return linkObject.linkinv(eta);
// 	}
//     
// 	//   mu.eta: function: derivative of the inverse-link function with
// 	//           respect to the linear predictor.  If the inverse-link
// 	//           function is mu = ginv(eta) where eta is the value of the
// 	//           linear predictor, then this function returns
// 	//           d(ginv(eta))/d(eta) = d(mu)/d(eta).
// 	inline glmColF mu_eta(const glmColF & eta) const{
// 		return linkObject.mu_eta(eta);
// 	}
	
	
	//DERIVED classes MUST define the following functions:
	
	// variance: function: the variance as a function of the mean.
	//inline Col<double> variance(const Col<double> & mu) const;
	
	// dev.resids: function giving the deviance for each observation as a
	//           function of ‘(y, mu, wt)’, used by the ‘residuals’ method
	//           when computing deviance residuals.
	//inline Col<double> dev_resids(const Col<double> & y,const Col<double> & mu, const Col<double> & wt) const;
	
	//      aic: function giving the AIC value if appropriate (but ‘NA’ for
	//           the quasi- families).  More precisely, this function returns
	//           -2 ll + 2 s, where ll is the log-likelihood and s is the
	//           number of estimated scale parameters.  Note that the penalty
	//           term for the location parameters (typically the “regression
	//           coefficients”) is added elsewhere, e.g., in ‘glm.fit()’, or
	//           ‘AIC()’, see the AIC example in ‘glm’.  See ‘logLik’ for the
	//           assumptions made about the dispersion parameter.
	//inline double aic(const Col<double> & y,const Col<int> & n,const Col<double> & mu,const Col<double> wt, double & dev) const;

	// initialize: expression.  This needs to set up whatever data objects are
	//           needed for the family as well as ‘n’ (needed for AIC in the
	//           binomial family) and ‘mustart’ (see ‘glm’).
	//inline void initialize(const Col<double> & y ,const Col<double> & start, const Col<double> & etastart,Col<int> & n,Col<double> & mustart) const;

	//  validmu: logical function.  Returns ‘TRUE’ if a mean vector ‘mu’ is
	//           within the domain of ‘variance’.
	//inline bool validmu(const Col<double> & mu) const;
	
	// valideta: logical function.  Returns ‘TRUE’ if a linear predictor ‘eta’
	//           is within the domain of ‘linkinv’.
	//inline bool valideta(const Col<double>  & eta) const;

	// simulate: (optional) function ‘simulate(object, nsim)’ to be called by
	//           the ‘"lm"’ method of ‘simulate’.  It will normally return a
	//           matrix with ‘nsim’ columns and one row for each fitted value,
	//           but it can also return a list of length ‘nsim’. Clearly this
	//           will be missing for ‘quasi-’ families.
};

