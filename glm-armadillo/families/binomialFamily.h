#pragma once
#include "baseFamily.h"
#include <boost/math/distributions/binomial.hpp>


template<class linkType>
 class binomialFamily:public baseFamily<linkType>{
public:
    mutable glmColF tdbinom;
	
	binomialFamily():baseFamily<linkType>("binomial"){
	}
	
	void dbinom(const glmMatI & x,const glmMatI & size, const glmMatF & prob,bool log_=false) const{
		//CHECK CONSISTENCY!!!
		glmMatF::const_iterator p=prob.begin();
		glmMatI::const_iterator s=size.begin();
		glmMatF::iterator r=tdbinom.begin();
		if(log_){
			for(const auto  & v:x){
				using boost::math::binomial;
				*r++=log(pdf(binomial(*s++,*p++),v));
			}
		}else{
			for(const auto  & v:x){
				using boost::math::binomial;
				*r++=pdf(binomial(*s++,*p++),v);
			}
		}
	}
	
	// variance: function: the variance as a function of the mean.
	inline glmColF variance(const glmColF & mu) const{
		return mu % (1 - mu);
	}
	
	// dev.resids: function giving the deviance for each observation as a
	//           function of ‘(y, mu, wt)’, used by the ‘residuals’ method
	//           when computing deviance residuals.
	glmColF y_log_y(const glmColF & y,const glmColF & mu) const{
		glmColF res(y.n_elem,arma::fill::zeros);
		arma::uvec good=find(y!=0.);
		res(good)=y(good) % arma::log(y(good)/mu(good));
		return res;
	}
	
	
	inline glmColF dev_resids(const glmColF & y,const glmColF & mu, const glmColF & wt) const{
		
		if(mu.n_elem != y.n_elem ){
			throw glmException("glmLinkLogit:dev_resids(...): mu must have the same length as n");
		}
		
 		if (wt.n_elem != y.n_elem){
			throw glmException("glmLinkLogit:dev_resids(...): wt must have the same length as n");
		}
		
		return 2 * wt % (y_log_y(y,mu) + y_log_y(1 - y,1-mu) );

// 		double y_log_y(double y, double mu){
// 			return (y != 0.) ? (y * log(y/mu)) : 0;
// 		}
// 		
// 		int i, n = LENGTH(y), lmu = LENGTH(mu), lwt = LENGTH(wt), nprot = 1;
// 		SEXP ans;
// 		double mui, yi, *rmu, *ry, *rwt, *rans;
// 
// 		if (!isReal(y)) {y = PROTECT(coerceVector(y, REALSXP)); nprot++;}
// 		ry = REAL(y);
// 		ans = PROTECT(shallow_duplicate(y));
// 		rans = REAL(ans);
// 		if (!isReal(mu)) {mu = PROTECT(coerceVector(mu, REALSXP)); nprot++;}
// 		if (!isReal(wt)) {wt = PROTECT(coerceVector(wt, REALSXP)); nprot++;}
// 		rmu = REAL(mu);
// 		rwt = REAL(wt);
// 		if (lmu != n && lmu != 1) error(_("argument %s must be a numeric vector of length 1 or length %d"),"mu", n);
// 		if (lwt != n && lwt != 1) error(_("argument %s must be a numeric vector of length 1 or length %d"),"wt", n);
// 		// Written separately to avoid an optimization bug on Solaris cc 
// 		if(lmu > 1) {
// 			for (i = 0; i < n; i++) {
// 				mui = rmu[i];
// 				yi = ry[i];
// 				rans[i] = 2 * rwt[lwt > 1 ? i : 0] *  (y_log_y(yi, mui) + y_log_y(1 - yi, 1 - mui));
// 			}
// 		} else {
// 			mui = rmu[0];
// 			for (i = 0; i < n; i++) {
// 				yi = ry[i];
// 				rans[i] = 2 * rwt[lwt > 1 ? i : 0] * (y_log_y(yi, mui) + y_log_y(1 - yi, 1 - mui));
// 			}
// 		}
// 
// 		UNPROTECT(nprot);
// 		return ans;
	}
	
	//      aic: function giving the AIC value if appropriate (but ‘NA’ for
	//           the quasi- families).  More precisely, this function returns
	//           -2 ll + 2 s, where ll is the log-likelihood and s is the
	//           number of estimated scale parameters.  Note that the penalty
	//           term for the location parameters (typically the “regression
	//           coefficients”) is added elsewhere, e.g., in ‘glm.fit()’, or
	//           ‘AIC()’, see the AIC example in ‘glm’.  See ‘logLik’ for the
	//           assumptions made about the dispersion parameter.
	inline glmFType aic(const glmColF & y,const glmColI & n,const glmColF & mu,const glmColF & wt, glmFType dev) const{
		glmFType ret;
		glmColF m;
		if (arma::any(n > 1)){
			//m=n;
			dbinom( n % arma::conv_to< glmColI >::from( y )  , n , mu ,true);
			ret = -2 * arma::sum( wt/n % tdbinom );
		}else{
		  //m=wt
		  dbinom( arma::conv_to< glmColI >::from( wt % y) , arma::conv_to< glmColI >::from(wt) , mu ,true);
		  ret= -2 * arma::sum(tdbinom);
		}
		return ret;
	}

	// initialize: expression.  This needs to set up whatever data objects are
	//           needed for the family as well as ‘n’ (needed for AIC in the
	//           binomial family) and ‘mustart’ (see ‘glm’).
	inline void initialize(const glmColF & y ,const glmColF & wt,const glmColF & start, const glmColF & etastart, glmColI & n, glmColF & mustart) const{
		n = glmColI(y.n_rows,arma::fill::ones);
		if (arma::any(y < 0) || arma::any(y > 1)){
			throw glmException("y values must be 0 <= y <= 1");
		}
		if(mustart.n_elem==0){
			mustart = (wt % y + 0.5)/(wt+1);
		}
		tdbinom=glmColF(y.n_rows);
		
// 	if (NCOL(y) == 1) {
// 		if (is.factor(y)) y <- y != levels(y)[1L]
// 		n <- rep.int(1, nobs)
// 		y[weights == 0] <- 0
// 		if (any(y < 0 | y > 1)) 
// 		stop("y values must be 0 <= y <= 1")
// 		mustart <- (wt * y + 0.5)/(weights + 1)
// 		m <- weights * y
// 		if (any(abs(m - round(m)) > 0.001)) 
// 		warning("non-integer #successes in a binomial glm!")
// 	}
	}

	//  validmu: logical function.  Returns ‘TRUE’ if a mean vector ‘mu’ is
	//           within the domain of ‘variance’.
	inline bool validmu(const glmColF & mu) const{
		return !mu.has_inf() && arma::all(mu > 0) && arma::all(mu<1);
	}
	
	// valideta: logical function.  Returns ‘TRUE’ if a linear predictor ‘eta’
	//           is within the domain of ‘linkinv’.
	inline bool valideta(const glmColF & eta) const{
		return true;
	}
	
	inline std::pair<glmFType,bool>  dispersion(const glmColF & weights, const glmColF & residuals,glmIType df_residuals){
		return std::make_pair(1.0,false);
	}

	// simulate: (optional) function ‘simulate(object, nsim)’ to be called by
	//           the ‘"lm"’ method of ‘simulate’.  It will normally return a
	//           matrix with ‘nsim’ columns and one row for each fitted value,
	//           but it can also return a list of length ‘nsim’. Clearly this
	//           will be missing for ‘quasi-’ families.
};
