#pragma once
//#include <iostream>
#include "glmSolver.h"
#include "../families/negativeBinomialFamily.h"
#include "../families/poissonFamily.h"

template<class linearSolverType, class linkType>
class glm_fit_nb:public glm_fit<linearSolverType,negativeBinomialFamily<linkType>>{
protected:
	
	glmFType i_theta=0;
	glmFType i_se_theta=0;
	glmFType i_twologlik=0;
	
public:
		
	inline std::pair<glmFType,glmFType> score_info(glmFType th, const glmColF & mu, const glmColF w) const{
		glmColF yth=(this->i_y + th);
		glmColF yth1=yth,yth2=yth;
		glmColF muth=(mu + th);
  		glmFType sc=arma::sum(w %  ( yth1.for_each( [](glmMatF::elem_type & val){ val = boost::math::digamma(val);}) - boost::math::digamma(th) + log(th) + 1 - arma::log(muth) -(yth)/(muth)));
  		glmFType in=arma::sum(w % ( -yth2.for_each( [](glmMatF::elem_type & val){ val = boost::math::trigamma(val);}) + boost::math::trigamma(th) -1/th +2/muth - yth/arma::square(muth)));
		
		//sum( w * ( -trigamma(th +  y) + trigamma(th) - 1/th + 2/(mu + th) - (y + th)/(mu + th)^2))
		
		return std::make_pair(sc,in);
	}
	
	inline glmFType loglik(glmFType th, const glmColF & mu,const glmColF & w) const{
		glmColF yth=(this->i_y + th);
		glmColF muth=(mu + th);
		return arma::sum(w % (arma::lgamma(yth) - lgamma(th) - arma::lgamma(this->i_y + 1) + th * log(th) + this->i_y % arma::log(mu + (this->i_y == 0)) - (yth) % arma::log(muth)));
	}
	
	inline std::pair<glmFType,glmFType> theta_ml(const glmColF  & mu, glmFType n,const glmColF weights,glmIType limit,glmFType eps,bool trace){
		glmFType t0 = n/arma::sum(weights % arma::square(this->i_y/mu - 1));
		glmIType it=0;
		glmFType dl=1;
		std::pair<glmFType ,glmFType > R;
		if (trace){
				std::cout << "theta.ml: initial theta = " << t0 << std::endl;
		}

 		while(it<limit && abs(dl) > eps) {
			t0 = abs(t0);
			R=score_info(t0, mu, weights);
			dl = R.first/R.second;
 			t0 = t0 + dl;
 			if (trace){
				std::cout << "theta.ml: iter " << it << " theta = " << t0 << " score=" << R.first << " info=" << R.second << std::endl;
			}
			it++;
 		}
 		
 		if (t0 < 0) {        
			t0 = 0;
			this->addWarning("estimate truncated at zero");
		}
		if (it == limit) {
			this->addWarning("iteration limit reached");
		}
		
		return std::make_pair(t0,R.second);
	}

	void initialize(glmIType nobs,glmIType nvars){
		if(!this->initialized()){
			glm_fit<linearSolverType,negativeBinomialFamily<linkType>>::initialize(nobs,nvars);
		}
	}
	
    glm_fit_nb():glm_fit<linearSolverType,negativeBinomialFamily<linkType>>(){
	}
	
    glm_fit_nb(glmIType nobs,glmIType nvars):glm_fit<linearSolverType,negativeBinomialFamily<linkType>>(){
		initialize(nobs,nvars);
	}
	
	
	glm_fit_nb(const glmMatF & x,const glmColF & y,const glmColF & weights, const glmColF & start,const glmColF & etastart, const glmColF & mustart, const glmColF & offset, const glmControl & control,bool intercept=true,bool singular_ok=true,glmFType init_theta=-1):glm_fit<linearSolverType,negativeBinomialFamily<linkType>>(){
		solve(x, y, weights, start, etastart, mustart, offset, control, intercept, singular_ok, init_theta);
	}

	void solve(const glmMatF & x,const glmColF & y, const glmColF & weights, const glmColF & start,const glmColF & etastart, const glmColF & mustart, const glmColF & offset, const glmControl & control,bool intercept=true,bool singular_ok=true,glmFType init_theta=-1){
		initialize(y.n_rows,x.n_cols);
		this->i_y=y;
		glmColF w;
		glmColF mu;
		glmColF eta;
		glmColF off;
		std::pair<glmFType,glmFType> th;
		auto n = this->i_y.n_rows;
		glmFType d1; 
		
		if(true){
			if (weights.n_elem==0){
				w =glmColF(n,arma::fill::ones);
			}else{
				if (arma::any(weights < 0)){
					throw glmException("negative weights not allowed");
				}else{
					w=weights;
				}
			}
			
			if (offset.n_elem==0){
				off = glmColF(n,arma::fill::zeros);
			}else{
				off=offset;
			}
			
			if(init_theta<0){
				glm_fit<linearSolverType,poissonFamily<linkType>> fit0(x,this->i_y,w,start,etastart,mustart,off,control,intercept,singular_ok);
				mu=fit0.i_fitted_values;
				th = theta_ml(mu, sum(w), w,control.i_maxit,pow(REG_EPS,0.25),control.i_trace);
				d1 = sqrt(2 * std::max(1, fit0.i_df_residual));
			}else{
				this->i_family.i_theta=init_theta;
				glm_fit<linearSolverType,negativeBinomialFamily<linkType>>::solve(x ,this->i_y ,w ,start,etastart,mustart,off,control,intercept,singular_ok);
				mu=this->i_fitted_values;
				th = theta_ml(mu, sum(w), w,control.i_maxit,pow(REG_EPS,0.25),control.i_trace);
				d1 = sqrt(2 * std::max(1, this->i_df_residual));
			}
			if(control.i_trace){
				std::cout << "Initial value for theta: " << th.first << std::endl;
			}
			
			glmFType t0;
			this->i_family.i_theta=th.first;
			glmIType out_iter = 0;
			glmFType d2=1;
			glmFType del=1;
			glmFType Lm = loglik(th.first, mu, w);
			glmFType Lm0 = Lm + 2 * d1;
			
			this->i_family.i_theta=th.first;
			while (out_iter<control.i_maxit && (abs(Lm0 - Lm)/d1 + abs(del)/d2) > control.i_glmTol) {
				eta =  this->i_family.linkfun(mu);
				glm_fit<linearSolverType,negativeBinomialFamily<linkType>>::solve(x ,this->i_y ,w ,glmColF(),eta,glmColF(),glmColF(),control,intercept,singular_ok);
				t0 = th.first;
				th = theta_ml(mu, sum(w), w, control.i_maxit, pow(REG_EPS,0.25), control.i_trace);
				this->i_family.i_theta=th.first;
				mu = this->i_fitted_values;
				
				del = t0 - th.first;
				Lm0 = Lm;
				Lm  = loglik(th.first, mu, w);
				if (control.i_trace) {
					glmFType Ls = loglik(th.first, this->i_y, w);
					glmFType Dev = 2 * (Ls - Lm);
					std::cout << "Theta(" << out_iter << ") = " << th.first << " " << Dev << std::endl;
				}
				out_iter++;
			}
			
			if (out_iter > control.i_maxit) {
				this->addWarning("alternation limit reached");
			}

			//NOTE QUESTO SEMBRA NON POTER SUCCEDERE MAIIII
// 			if (off.n_elem>0 && intercept) {
// 				if (x.n_cols<=1){
// 					this->i_null_deviance = this->i_deviance;
// 				}else{
// // 					startnull=Col<double>();
// // 					etastartnull=Col<double>();
// // 					mustartnull=Col<double>();
// // 					offsetnull=Col<double>();
// // 					glm_fit<negativeBinomialFamily<glmLinkLog>> nullFit(x.col(0) ,y ,w ,startnull,etastartnull,mustartnull, offsetnull, fam, control,intercept,singular_ok);
// // 					null_deviance = nullFit.deviance;
// 					//null.deviance <-if(length(Terms)) glm.fitter(X[, "(Intercept)", drop = FALSE], Y, w, offset = offset, family = fam, control = list(maxit = control$maxit, epsilon = control$epsilon, trace = control$trace > 1), intercept = TRUE)$deviance
// 					
// 					//this->null_deviance = fit.null_deviance;
// 
// 				}
// 			}
			
			this->i_aic = -(2 * Lm) + 2 * this->i_rank + 2;
			i_twologlik=2 * Lm;
			i_theta=th.first;
			i_se_theta=th.second;
		}else{
			
		}
		
	}

};
