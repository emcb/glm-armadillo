#pragma once
#include "../glmBase.h"

template<class linearSolverType,class familyType>
class glm_fit:public linearSolverType{
	
protected:

	glmColF i_linear_predictors;
	glmColF i_prior_weights;	
	glmColF i_weights;
	glmColF i_y;
	
	glmFType i_deviance=0;
	glmFType i_aic=0;	
	glmFType i_null_deviance=0;
	glmIType i_iter=0;

	glmIType i_df_null=0;

	bool i_converged=false;
	bool i_boundary=false;
	
	
	void initialize(glmIType nobs,glmIType nvars){
		if(!linearSolverType::initialized()){
			linearSolverType::initialize(nobs,nvars);
			i_fitted_values=glmColF(nobs);
			i_linear_predictors=glmColF(nobs);
			i_prior_weights=glmColF(nobs);
			i_weights=glmColF(nobs);
			i_y=glmColF(nobs);
		}
	}
	
	
public:
	familyType i_family;		
	glmIType i_df_residual=0;;	
	glmColF i_fitted_values;
	
	glm_fit():linearSolverType(){
	}
	
	glm_fit(glmIType nobs,glmIType nvars):linearSolverType(){
		initialize(nobs,nvars);
	}

	
	glm_fit(const glmMatF  & x,const glmColF & y,const glmColF & weights, const glmColF & start,const glmColF  & etastart,const glmColF & mustart, const glmColF & offset, const glmControl & control,bool intercept=true,bool singular_ok=true){
		solve(x,y,weights,start,etastart,mustart,offset,control,intercept,singular_ok);
	}
	
	//                                       = rep(1, nobs)       = NULL             = NULL              = NULL     = rep(0, nobs)
	inline void solve(const glmMatF  & x,const glmColF & y,const glmColF & weights, const glmColF & start, const glmColF  & etastart, const glmColF  & mustart, const glmColF & offset, const glmControl & control,bool intercept=true,bool singular_ok=true){
		glmAType nobs = y.n_rows;
		glmAType nvars = x.n_cols;
		initialize(nobs,nvars);
		i_y=y;
		bool EMPTY = (nvars == 0);
		
		glmColF off;
		glmColF mu;
		glmColF eta;
		glmColA good;
		glmColF w;
		glmFType dev;

		if (weights.n_elem==0){
			i_prior_weights = glmColF(nobs,arma::fill::ones);
		}else{
			i_prior_weights=weights;
		}
		if (offset.n_elem==0){
			off = glmColF(nobs,arma::fill::zeros);
		}else{
			off=offset;
		}
		
		glmColI  n;
		//this initializes n and mustart
		mu=mustart;
		//this initializes n and mustart if mustart is already specifiend (i.e. not empty then uses it DO NOT OVERWRITE!!!)
		i_family.initialize(i_y,i_prior_weights,start,etastart,n,mu);
		i_family.initialize_link(mu);


		if (EMPTY) {
			//eta=glmColF(nobs,arma::fill::zeros)+offset;
			eta=glmColF(nobs,arma::fill::zeros)+off;
			if (!i_family.valideta(eta)){
				throw glmException("invalid linear predictor values in empty model");
			}
			
			mu = i_family.linkinv(eta);
			if (!i_family.validmu(mu)){
				throw glmException("invalid fitted means in empty model");
			}
			
			glmFType dev = arma::sum(i_family.dev_resids(y, mu, i_prior_weights));
			w = arma::sqrt((i_prior_weights % arma::square(i_family.mu_eta(eta)))/i_family.variance(mu));
			this->i_residuals = (y - mu)/i_family.mu_eta(eta);
			good = glmColA(this->i_residuals.n_elem,arma::fill::ones);
			i_boundary = true;
			i_converged = true;
			i_iter = 0;
			this->i_rank = 0;
		}else{
			glmColF coefold;
			if(etastart.n_elem>0){
				eta=etastart;
			}else{
				if(start.n_elem>0){
					if(start.n_elem!=nvars){
						throw glmException("length of 'start' should equal X columns");
					}
					coefold = start;
					//eta=offset + x * start;
					eta=off + x * start;
				}else{
					eta=i_family.linkfun(mu);
				}
			}
			mu =i_family.linkinv(eta);
		
			if (!(i_family.validmu(mu) && i_family.valideta(eta))){
				throw glmException("cannot find valid starting values: please specify some");
			}
			glmFType devold = sum(i_family.dev_resids(y, mu, i_prior_weights));
			i_boundary=false;
			i_converged=false;
			
			//start=glmColF(nvars,arma::fill::zeros);
			this->i_coefficients=glmColF(nvars,arma::fill::zeros);

			//fit_object fit(nvars,1,min(1e-07, control.tol/1000));
			//linear_fit_object fit(nvars,1,std::min(1e-07, control.i_glmTol/1000));

			for (i_iter =1;i_iter<=control.i_maxit;i_iter++){
				good = arma::find(i_prior_weights > 0);
				glmColF varmu = i_family.variance(mu.elem(good));
				if (varmu.has_nan()){
					throw glmException("NAs in V(mu)");
				}

				if (arma::any(varmu == 0)){
					throw glmException("0s in V(mu)");
				}
				
				glmColF mu_eta_val = i_family.mu_eta(eta.elem(good));
				if(mu_eta_val.has_nan()){
					throw glmException("NAs in d(mu)/d(eta)");
				}
				glmColA muetavalgood=arma::find(mu_eta_val!=0);
				//good = good.elem(arma::find(mu_eta_val!=0));
				good = good.elem(muetavalgood);


				if (good.n_elem==0) {
					this->addWarning("no observations informative at iteration iter");
					break;
				}
				
				glmColF z = (eta(good) - off(good)) + (y(good) - mu(good))/mu_eta_val(muetavalgood);
				w = arma::sqrt((i_prior_weights(good) % arma::square(mu_eta_val(muetavalgood)))/i_family.variance(mu(good)));
				glmColF ty=z % w;
				
				linearSolverType::solve(((glmMatF)x.rows(good)).each_col() % w,ty,false);
				
				
				if (this->i_coefficients.has_inf()){
					this->addWarning("non-finite coefficients at iteration iter");
					break;
				}
				
				if (nobs < this->i_rank){
					throw glmException("X matrix has rank fit$rank, but onlynobs observations");
				}
				
				if (!singular_ok && this->i_rank < nvars){
					throw glmException("singular fit encountered");
				}
				eta = x * this->i_coefficients + off;
				mu = i_family.linkinv(eta);
				
				dev = arma::sum(i_family.dev_resids(y, mu, i_prior_weights));
				
				if (control.i_trace){
					std::cout << "Deviance = " << dev << " Iterations - " << i_iter << std::endl;
				}
				i_boundary = false;
				
				if (!std::isfinite(dev)) {
					if (coefold.n_elem==0){
						throw glmException("no valid set of coefficients has been found: please supply starting values");
					}
					this->addWarning("step size truncated due to divergence");
					int ii = 1;
					while (!std::isfinite(dev)) {
						if (ii > control.i_maxit){
							throw glmException("inner loop 1; cannot correct step size");
						}
						ii++;
						this->i_coefficients = (this->i_coefficients + coefold)/2;
						eta = x * this->i_coefficients + off;
						mu = i_family.linkinv(eta);
						dev = arma::sum(i_family.dev_resids(y, mu, i_prior_weights));
					}
					i_boundary = true;
					if (control.i_trace){
						std::cout << "Step halved: new deviance = " << dev << std::endl;
					}
				}

				if (!(i_family.valideta(eta) && i_family.validmu(mu))) {
					if (coefold.n_elem==0){
						throw glmException("no valid set of coefficients has been found: please supply starting values"); 
					}
						
					this->addWarning("step size truncated: out of bounds");
					
					int ii = 1;
					while (!(i_family.valideta(eta) && i_family.validmu(mu))) {
						if (ii > control.i_maxit){ 
							throw glmException("inner loop 2; cannot correct step size");
						}
						ii++;
						this->i_coefficients = (this->i_coefficients + coefold)/2;
						eta = x * this->i_coefficients + off;
						mu = i_family.linkinv(eta);
					}
					i_boundary = true;
					dev = arma::sum(i_family.dev_resids(y, mu, i_prior_weights));
					if (control.i_trace){
						std::cout << "Step halved: new deviance = " <<  dev << std::endl;
					}
				}
				
				if (abs(dev - devold)/(0.1 + abs(dev)) < control.i_glmTol) {
					i_converged = true;
					break;
				} else {
					devold = dev;
					coefold =this->i_coefficients;
				}
			}

			
			if (!i_converged){
				this->addWarning("glm.fit: algorithm did not converge");
			}
			
			if (i_boundary){
				this->addWarning("glm.fit: algorithm stopped at boundary value");
			}
			
			glmFType eps = 10 * REG_EPS;
			if (i_family.getFamilyName() == "binomial" && (arma::any(mu > (1 - eps)) || arma::any(mu < eps))) {
				this->addWarning("glm.fit: fitted probabilities numerically 0 or 1 occurred");
			}
			if (i_family.getFamilyName() == "poisson"  && arma::any(mu < eps) ){
				this->addWarning("glm.fit: fitted rates numerically 0 occurred"); 
			}
			if (this->i_rank < nvars){
				this->i_coefficients(this->i_pivot(arma::linspace<glmColA>((glmAType)this->i_rank,(glmAType)(nvars-1)))).fill(arma::datum::nan);
			}
			this->i_residuals = (y - mu)/i_family.mu_eta(eta);
			
			i_deviance=dev;
		}
		
		i_weights=glmColF(nobs,arma::fill::zeros);
		i_weights(good)=arma::square(w);

		glmColF wtdmu;
		
		if(intercept){
			wtdmu=glmColF(nobs,arma::fill::ones);
			wtdmu.fill(arma::sum(i_prior_weights % y)/arma::sum(i_prior_weights));
		}else{
			wtdmu=i_family.linkinv(off);
		}
		i_null_deviance = arma::sum(i_family.dev_resids(y, wtdmu, i_prior_weights));
		
		glmIType n_ok = nobs - sum(i_prior_weights == 0);
		i_df_null = n_ok - intercept;
		i_df_residual = n_ok - this->i_rank;
		i_aic = i_family.aic(y, n, mu, i_prior_weights, i_deviance) + 2 * this->i_rank;
		i_fitted_values = mu;
		i_linear_predictors=eta;
		
// 		if(!EMPTY){
// 			effects=fit.effects;
// 		}
		
		///MANCA RMAT!!!!!!!!
// 		list(
// 			coefficients = coef, 
// 			residuals = residuals, 
// 			fitted.values = mu, 
// 			effects = if (!EMPTY) fit$effects, 
// 			R = if (!EMPTY) Rmat, 
// 			rank = rank, 
// 			qr = if (!EMPTY) structure(fit[c("qr", "rank", "qraux", "pivot", "tol")], class = "qr"), 
// 			family = family, 
// 			linear.predictors = eta, 
// 			deviance = dev, 
// 			aic = aic.model, 
// 			null.deviance = nulldev, 
// 			iter = iter, 
// 			weights = wt, 
// 			prior.weights = weights, 
// 			df.residual = resdf, 
// 			df.null = nulldf, 
// 			y = y, 
// 			converged = conv, 
// 			boundary = boundary
// 		)
	}
	
	glmMatF  coeffTable(){
		std::pair<glmFType,bool> dispersion=i_family.dispersion(i_weights,this->i_residuals,i_df_residual);
		return linearSolverType::coeffTable(i_df_residual,dispersion.first,dispersion.second);
	}
	
	void summary(){
		std::cerr << "    Null deviance: " << i_null_deviance <<  " on " << i_df_null << " degrees of freedom" << std::endl;	
		std::cerr << "Residual deviance: " << i_deviance <<  " on " << i_df_residual << " degrees of freedom" << std::endl;
		std::cerr << "AIC: " << i_aic << std::endl;
		std::cerr << "Number of Fisher Scoring Iterations: " << i_iter << std::endl;
	}

};

