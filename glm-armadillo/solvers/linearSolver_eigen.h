#pragma once
#include "baseSolver.h"
#include <boost/math/distributions/students_t.hpp>
#include <boost/math/distributions/normal.hpp>

// #include "../../Eigen/Core"
// #include "../../Eigen/QR"
// #include "../../Eigen/LU" 

#include <Eigen/Core>
#include <Eigen/QR>
#include <Eigen/LU> 


typedef Eigen::Matrix<glmFType,Eigen::Dynamic,Eigen::Dynamic> eglmMatF;
typedef Eigen::Matrix<glmFType,Eigen::Dynamic,1> eglmColF;
typedef Eigen::Matrix<glmAType, Eigen::Dynamic, 1> eglmColA;



/*
Eigen::MatrixXd example_cast_eigen(arma::mat arma_A) {

  Eigen::MatrixXd eigen_B = Eigen::Map<Eigen::MatrixXd>(arma_A.memptr(),arma_A.n_rows,arma_A.n_cols);

  return eigen_B;
}

arma::mat example_cast_arma(Eigen::MatrixXd eigen_A) {

  arma::mat arma_B = arma::mat(eigen_A.data(), eigen_A.rows(), eigen_A.cols(),false, false);

  return arma_B;
}
*/

class linearSolver:public baseSolver{
	
public:
	Eigen::ColPivHouseholderQR<Eigen::Matrix<glmFType,Eigen::Dynamic,Eigen::Dynamic>> QR;
	glmMatF covmat;
	//glmColF i_effects;
	glmColF i_residuals;
	glmColA i_pivot;
	
		inline void initialize(glmIType nobs,glmIType nvars){
		if(!initialized()){
			baseSolver::initialize(nobs,nvars);
			QR=Eigen::ColPivHouseholderQR<Eigen::Matrix<glmFType,Eigen::Dynamic,Eigen::Dynamic>>(nobs,nvars);
			i_pivot=arma::linspace<glmColA>(1,nvars,nvars);
			//i_effects=glmColF(nobs); 
			i_residuals=glmColF(nobs);
		}
	}

	
	
	linearSolver(){
	}
	
	linearSolver(glmIType nobs,glmIType nvars){
		initialize(nobs,nvars);
	}

	linearSolver(const glmMatF & x,const glmMatF & y, const glmControl & control){
		initialize(y.n_rows,x.n_cols);
		this->solve(x,y,control);
	}
	
	inline void solve(const glmMatF & x,const glmMatF & y, const glmControl & control){
		if(control.i_check){
			if (x.n_rows != y.n_elem) {
				throw glmException("Error: Dimensions of x and y do not match");//printf("Error: Dimensions of \'x\' (%d,%d) and \'y\' (%d) do not match", n, p, (int)y.size());
			}
			
			if( x.has_inf() ){
				throw glmException("Error: NA/NaN/Inf in x matrix");//printf("Error: NA/NaN/Inf in \'x\' matrix: (%d,%d)\n", i, j);
			}
			
			if( y.has_inf() ){
				throw glmException("Error: NA/NaN/Inf in y matrix");//printf("Error: NA/NaN/Inf in \'y\' vector: element %d\n", i);
			}
		}
		
		initialize(y.n_rows,x.n_cols);
		//cout << "Using eigen solver..." << endl;
		eglmMatF ex = Eigen::Map<const eglmMatF>(x.memptr(),x.n_rows,x.n_cols);
		eglmColF ey = Eigen::Map<const eglmColF>(y.memptr(),y.n_rows,y.n_cols);
		auto n = x.n_rows;
		auto p = x.n_cols;

		QR.setThreshold(control.i_lmTol);
		QR.compute(ex);
		this->i_rank=QR.rank();
// 		auto qrm=QR.matrixQR();
// 		qr=glmMatF(qrm.data(), qrm.rows(), qrm.cols(),true, false);
 		eglmColA pv=QR.colsPermutation().indices().cast<glmAType>();
		i_pivot = glmColA(pv.data(), pv.rows());
		eglmColF s=QR.solve(ey);
		this->i_coefficients=glmColF(s.data(), s.rows());
		i_residuals = y - (x * this->i_coefficients);
	}

	glmMatF coeffTable(int df_residual,double dispersion,bool estimated){
		eglmMatF R = QR.matrixQR().block(0,0,this->i_rank,this->i_rank).triangularView<Eigen::Upper>();
		eglmMatF A=R.transpose() * R;
		eglmMatF tcm=A.llt().solve(eglmMatF::Identity(A.rows(),A.cols()));
		glmMatF covmat(tcm.data(), tcm.rows(), tcm.cols(),true, false);

		glmColA p1=arma::linspace<glmColA>(0,this->i_rank-1,this->i_rank);
		glmColA sel=i_pivot(p1);
		glmColA IND({0,1,2,3});
		
		glmMatF table(i_coefficients.n_elem,4);
 		table.fill(arma::datum::nan);
		table.col(0)=i_coefficients;
		table(sel,IND.row(1))=sqrt(diagvec(covmat * dispersion));
		table(sel,IND.row(2))=table(sel,IND.row(0))/table(sel,IND.row(1));				

		if(estimated){
			for(int i=0;i<sel.n_rows;i++){
				using boost::math::students_t;
				table.col(3)(sel(i)) = 2 * cdf(students_t(df_residual),-abs(table.col(2)(sel(i))) );
			}
		}else{
			for(int i=0;i<sel.n_rows;i++){
				using boost::math::normal_distribution;
				table.col(3)(sel(i)) = 2 * cdf(normal_distribution<glmFType>(),-abs(table.col(2)(sel(i))));
			}				
		}

		return table;
	}

};
