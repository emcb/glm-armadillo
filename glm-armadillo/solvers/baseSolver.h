#pragma once
#include <iostream>

#include "../glmBase.h"

class baseSolver{
private:
	bool i_initialized=false;
protected:
	glmColF i_coefficients;
	glmIType i_rank=0;
	std::vector<std::string> i_warnings;
	
public:
	void initialize(glmIType nobs,glmIType nvars){
		if(!i_initialized){
			i_coefficients=glmColF(nvars);
			i_initialized=true;
		}
	}
	
	baseSolver(){}
	
	baseSolver(glmIType nobs,glmIType nvars){
		initialize(nobs,nvars);
	}

	inline const bool & initialized(){
		return i_initialized;
	}
	
	
	inline void addWarning(const std::string & warn){
		std::cout << "Warning: " << warn << std::endl; 
		i_warnings.push_back(warn);
	}
	
	inline const std::vector<std::string> & getWarnings() const{
		return i_warnings;
	}
	
	
	inline const glmColF & Coefficients(){
		return i_coefficients;
	}
	
};



