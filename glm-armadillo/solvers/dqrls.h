extern "C" {
	void dqrls_(double *x, const int *n, const int *p, const double *y, const int *ny, const double *tol, double *b, double *rsd, double *qty, int *k, int *jpvt, double *qraux, double *work);
}
