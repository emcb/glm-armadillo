#include "../glmBase.h"

void givens(glmMatF & L, glmAType & k){
  glmAType p = L.n_rows;
  glmAType n = L.n_cols;
	if (p != n) 
		throw glmException("Wrong Matrix");
	if (k > p)
		throw glmException("Wrong input of k");
	L.shed_row(k);

	glmAType mk = k;
	while(mk < p-1){
		glmColF mx = {L(mk,mk),L(mk,mk+1)};
		glmFType lmx   = sqrt(sum(mx % mx));
		L(mk,mk)    = lmx;
		L(mk,mk+1)  = 0;
		glmMatF gives = {{mx(0)/lmx, -mx(1)/lmx},{mx(1)/lmx,mx(0)/lmx}};

		if(mk < p-2)
			L.submat(mk+1,mk,p-2,mk+1) = L.submat(mk+1,mk,p-2,mk+1) * gives;
		mk += 1;
	}
	L.shed_col(p-1);
}


void forupdate(glmMatF L, glmColF & xxk, glmFType & xkxk){
  glmColF lk = arma::solve(arma::trimatl(L), xxk);
  glmFType lkk   = sqrt(xkxk - sum(lk % lk));

  lk.resize(lk.size()+1);
  lk[lk.size()-1] = lkk;

  glmColF zero(L.n_rows, arma::fill::zeros);
  glmMatF LL = arma::join_rows(L,zero);

  L = arma::join_cols(LL, lk.t());
}




void PushBack(glmColA & A, glmAType & j){
	glmAType p = A.size();
	A.resize(p+1);
	A(p)  = j;
}


Rcpp::List elasticnet(glmMatF & XTX, glmColF & XTY,	glmFType lam2 ,glmFType lam1 = -1){

	glmAType p = XTX.n_rows;
	glmColF w    = sqrt(XTX.diag(0));
	glmColF w1   = 1/w;
	glmColF w2   = XTX.diag();
	glmFType alpha0  = 1/(1 + lam2);
	glmFType alpha1  = 1 - alpha0;
	glmMatF WW   = arma::diagmat(w2);
	glmMatF XTXW = alpha0 * XTX + alpha1 * WW;

	Rcpp::LogicalVector A(p);
	A.fill(false);

	glmColF absXTY = arma::abs(XTY) % w1;
	glmAType j    = absXTY.index_max();
	A(j) = !A(j);

	glmColA VA = {j};
	glmMatF L   = {w(j)};
	glmFType lamb   = w1(j) * std::abs(XTY(j));

	glmColF b(p);
	b.fill(0);
	Rcpp::NumericVector relamb = {lamb};
	glmMatF reb(b.t());

	glmColA VAm = arma::linspace<arma::uvec>(0,p-1,p);
	VAm.shed_row(j);

	glmColA CC(p), SCC(p);

	while(true){
		CC  = w1 % (XTY - XTXW * b);
		SCC = arma::sign(CC);

		glmColF SCCA = SCC(VA);
		glmColF td   = arma::solve(trimatl(L),w(VA) % SCCA);
		glmColF d    = arma::solve(trimatu(L.t()),td);
		glmMatF XTX_ = XTX.submat(VAm, VA);
		glmColF a    = alpha0 * w1(VAm) % (XTX_ * d);
		glmColF gam(p, arma::fill::zeros);
		glmColF ww   = -b(VA)/d;

		for(glmAType i = 0; i < VA.size(); i++){
			if(ww(i) > 0 && ww(i) < lamb)
				gam(VA(i)) = ww(i);
			else
				gam(VA(i)) = lamb;
		}

		if(sum(A) < p){
			for(glmAType i = 0; i < (p-VA.size()); i++){
				if(a(i) * lamb <= CC(VAm(i)))
					gam(VAm(i)) = (lamb - CC(VAm(i)))/(1-a(i));
				else
					gam(VAm(i)) = (lamb + CC(VAm(i)))/(1+a(i));
			}
		}

		j = gam.index_min();
		glmFType gammin = gam(j);
		if(lam1 >= 0 && lamb - gammin <= lam1){
			b(VA) = b(VA) + (lamb - lam1) * d;
			Rcpp::List res2 = Rcpp::List::create(Rcpp::Named("b") = b);
			return(res2);
		}
		b(VA) = b(VA) + gammin * d;
		lamb  = lamb - gammin;
		relamb.push_back(lamb);
		reb   = arma::join_cols(reb,b.t());

		if(lamb == 0)
			break;

		glmColA jj;
		for(glmAType i = 0; i < VA.size(); i++){
			if(VA[i] == j)
				PushBack(jj, i);
		}

		if(jj.size() == 0){
			glmColA j_   = {j};
			glmColF XTXAJ = alpha0 * XTX(VA, j_);
			glmFType XTXjj    = w2(j);

			forupdate(L,XTXAJ,XTXjj);
			PushBack(VA,j);

			glmColA del = find(VAm == j);
			VAm.shed_row(del(0));
		}
		else{
			givens(L,jj(0));
			PushBack(VAm, VA(jj(0)));
			VA.shed_row(jj(0));
		}
		A(j) = !A(j);
	}
	Rcpp::List res = Rcpp::List::create(Rcpp::Named("reb") = reb , Rcpp::Named("relamb") = relamb);
	return(res);
}
