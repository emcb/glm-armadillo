#pragma once
#include "baseSolver.h"
#include "dqrls.h"
#include <boost/math/distributions/students_t.hpp>
#include <boost/math/distributions/normal.hpp>

class linearSolver:public baseSolver{
protected:
	glmColI i_tpivot;
	glmColF i_work;
	glmMatF i_qr;
	glmColF i_qraux;
	glmColF i_effects;	
	
	glmColA i_pivot;	
	glmColF i_residuals;	
	
public:
	
	inline void initialize(glmIType nobs,glmIType nvars){
		if(!initialized()){
			baseSolver::initialize(nobs,nvars);
			i_work=glmColF(2*nvars);
			i_qraux=glmColF(nvars);
			i_qr=glmMatF(nobs,nvars);
			i_effects=glmColF(nobs); 
			i_residuals=glmColF(nobs); 
		}
	}
	
	linearSolver(){
	}
	
	linearSolver(glmIType nobs,glmIType nvars){
		initialize(nobs,nvars);
	}

	linearSolver(const glmMatF & x,const glmMatF & y, const glmControl & control){
		initialize(y.n_rows,x.n_cols);
		this->solve(x,y,control);
	}
	
	inline void solve(const glmMatF & x,const glmMatF & y, const glmControl & control){
		
		if(control.i_check){
			if (x.n_rows != y.n_elem) {
				throw glmException("Error: Dimensions of x and y do not match");//printf("Error: Dimensions of \'x\' (%d,%d) and \'y\' (%d) do not match", n, p, (int)y.size());
			}
			
			if( x.has_inf() ){
				throw glmException("Error: NA/NaN/Inf in x matrix");//printf("Error: NA/NaN/Inf in \'x\' matrix: (%d,%d)\n", i, j);
			}
			
			if( y.has_inf() ){
				throw glmException("Error: NA/NaN/Inf in y matrix");//printf("Error: NA/NaN/Inf in \'y\' vector: element %d\n", i);
			}
		}
		
		initialize(y.n_rows,x.n_cols);
		int n = x.n_rows;
		int p = x.n_cols;
		int ny=1;
		i_tpivot=arma::linspace<glmColI>(1,p,p);		
		i_qr=x;
		dqrls_(i_qr.memptr(), &n, &p, y.memptr(), &ny, &control.i_lmTol, i_coefficients.memptr(), i_residuals.memptr(), i_effects.memptr(), &i_rank, i_tpivot.memptr(), i_qraux.memptr(), i_work.memptr());
		i_pivot = arma::conv_to<glmColA>::from(i_tpivot-1);
		i_coefficients(i_pivot)=i_coefficients;
		i_residuals=x * this->i_coefficients;
		
	}
	
	inline glmMatF coeffTable(glmIType df_residual,glmFType dispersion,bool estimated){
		glmColA p1=arma::linspace<glmColA>((glmAType)0,(glmAType)(i_rank-1),i_rank);
		glmColA sel=i_pivot(p1);
		glmColA IND({0,1,2,3});
		glmMatF R=trimatu(i_qr(p1,p1));
		
		glmMatF table(i_coefficients.n_elem,4);
 		table.fill(arma::datum::nan);
		table.col(0)=i_coefficients;
		table(sel,IND.row(1))=sqrt(diagvec(arma::inv_sympd(R.t() * R) * dispersion));
		table(sel,IND.row(2))=table(sel,IND.row(0))/table(sel,IND.row(1));		

		if(estimated){
			for(int i=0;i<sel.n_rows;i++){
				using boost::math::students_t;
				table.col(3)(sel(i)) = 2 * cdf(students_t(df_residual),-abs(table.col(2)(sel(i))) );
			}
		}else{
			for(int i=0;i<sel.n_rows;i++){
				using boost::math::normal_distribution;
				table.col(3)(sel(i)) = 2 * cdf(normal_distribution<glmFType>(),-abs(table.col(2)(sel(i))));
			}				
		}

		return table;
	}
	
};
