#pragma once
#include "../glmBase.h"

#include <Eigen/Core>
#include <Eigen/QR>

typedef Eigen::Matrix<glmFType,Eigen::Dynamic,Eigen::Dynamic> eglmMatF;
typedef Eigen::Array<glmFType,Eigen::Dynamic,1> eglmAColF;
typedef Eigen::Array<glmFType,Eigen::Dynamic,Eigen::Dynamic> eglmAMatF;
typedef Eigen::Matrix<glmAType, Eigen::Dynamic, 1> eglmColA;

inline glmMatF poly(glmColF x,glmIType deg,bool raw=false){
	eglmAColF ex = Eigen::Map<const eglmAColF>(x.memptr(),x.n_rows,1);
	glmFType xbar=ex.mean();
	ex -= xbar;
	eglmAMatF X=eglmAMatF::Ones(ex.rows(),deg+1); 
	for(glmAType i=1;i<=deg;i++){
		X.col(i)=X.col(i-1) * ex;
	}
	eglmMatF Z(X.rows(),X.cols());
	if(!raw){
		Eigen::ColPivHouseholderQR<eglmMatF> QR(X.matrix());
		if (QR.rank() < deg){
			throw glmException("'degree' must be less than number of unique points");
		}

		eglmMatF z=eglmMatF::Zero(X.cols(),X.cols());
		z.diagonal()=QR.matrixQR().diagonal();
		eglmMatF Q=QR.matrixQ();
		Z = Q.leftCols(X.cols()) * z;

		Z.colwise().normalize();
		Z=Z.rightCols(deg);
		//eglmAColF alpha <- (colSums(x * Z^2)/norm2 + xbar)[1L:degree]
		//norm2 <- c(1, norm2)	
	}else{
		Z=X;
	}
	glmMatF Ret=glmMatF(Z.data(), Z.rows(),Z.cols());
	return Ret;
}

