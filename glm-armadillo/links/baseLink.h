#pragma once
#include "../glmBase.h"
#include <string>

////////LINKS
//      gaussian(link = "identity")
//      inverse.gaussian(link = "1/mu^2")
//      poisson(link = "log")
//      binomial(link = "logit")

//      Gamma(link = "inverse")
//      quasi(link = "identity", variance = "constant")
//      quasibinomial(link = "logit")
//      quasipoisson(link = "log")


class baseLink{
protected:
	
	std::string name;
	
	baseLink(){
	}
	baseLink(const std::string & name_):name(name_){
	}
public:
	inline const std::string & getLinkName() const{
		return name;
	}
	
	//Derived classes MUST define the following functions:
	
	//  linkfun: function: the link.
	//inline Col<double> linkfun(const Col<double> & mu) const;
	
	//  linkinv: function: the inverse of the link function.
	//inline Col<double> linkinv(const Col<double> & eta) const;
	
	//   mu.eta: function: derivative of the inverse-link function with
	//           respect to the linear predictor.  If the inverse-link
	//           function is mu = ginv(eta) where eta is the value of the
	//           linear predictor, then this function returns
	//           d(ginv(eta))/d(eta) = d(mu)/d(eta).
	//inline Col<double> mu_eta(const Col<double> & eta) const;
};
