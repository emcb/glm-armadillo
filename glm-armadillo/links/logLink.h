#pragma once
#include "baseLink.h"

class logLink:public baseLink{
private:
	glmColF small;
public:
	logLink():baseLink("log"){
		
	}
	
	inline void initialize_link(const glmColF & mu){
		small=glmColF(mu.n_elem).fill(REG_EPS);
	}
	
	//  linkfun: function: the link.
	inline glmColF linkfun(const glmColF & mu) const{
		return arma::log(mu);
	}
	
	//  linkinv: function: the inverse of the link function.
	inline glmColF linkinv(const glmColF & eta) const {
		return arma::max(arma::exp(eta),small);
	}
	
	//   mu.eta: function: derivative of the inverse-link function with
	//           respect to the linear predictor.  If the inverse-link
	//           function is mu = ginv(eta) where eta is the value of the
	//           linear predictor, then this function returns
	//           d(ginv(eta))/d(eta) = d(mu)/d(eta).
	inline glmColF mu_eta(const glmColF & eta) const{
		return arma::max(arma::exp(eta),small);
	}

};
