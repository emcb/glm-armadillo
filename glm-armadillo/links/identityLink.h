#pragma once
#include "baseLink.h"

class identityLink:public baseLink{
public:
	identityLink():baseLink("identity"){
	}
	
	inline void initialize_link(const glmColF & mu){
	}


	//  linkfun: function: the link.
	inline glmColF linkfun(const glmColF & mu) const{
		return mu;
	}
	
	//  linkinv: function: the inverse of the link function.
	inline glmColF linkinv(const glmColF & eta) const {
		return eta;
	}
	
	//   mu.eta: function: derivative of the inverse-link function with
	//           respect to the linear predictor.  If the inverse-link
	//           function is mu = ginv(eta) where eta is the value of the
	//           linear predictor, then this function returns
	//           d(ginv(eta))/d(eta) = d(mu)/d(eta).
	inline glmColF mu_eta(const glmColF & eta) const{
			return glmColF(eta.n_rows,arma::fill::ones);
	}
};
