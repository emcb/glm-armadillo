#pragma once
#include "baseLink.h"

class logitLink:public baseLink{
public:
	logitLink():baseLink("logit"){
		
	}
	
	inline void initialize_link(const glmColF & mu){

	}
	
	//  linkfun: function: the link.
	glmColF linkfun(const glmColF & mu) const{
		if(arma::any(mu<0) || arma::any(mu>1)){
			throw glmException("mu out of range (0, 1)");
		}
		//Col<double> x_d_omx=mu/(1 - mu);
		return arma::log(mu/(1 - mu));
		
// 	static R_INLINE double x_d_omx(double x) {
// 		if (x < 0 || x > 1)
// 		error(_("Value %g out of range (0, 1)"), x);
// 		return x/(1 - x);
// 	}
		
// 		int i, n = LENGTH(mu);
// 		SEXP ans = PROTECT(shallow_duplicate(mu));
// 		double *rans = REAL(ans), *rmu=REAL(mu);
// 
// 		if (!n || !isReal(mu))
// 		error(_("Argument %s must be a nonempty numeric vector"), "mu");
// 		for (i = 0; i < n; i++)
// 		rans[i] = log(x_d_omx(rmu[i]));
// 		UNPROTECT(1);
// 		return ans;
		
	}
	
	
	//  linkinv: function: the inverse of the link function.
	glmColF linkinv(const glmColF & eta) const {
		glmColF res(eta.n_elem);
		
#pragma omp parallel for
		for (int i = 0; i < eta.n_elem ; i++) {
			glmFType tmp;
			tmp = (eta[i] < -30.) ? REG_EPS : ((eta[i] > 30.) ? ((glmFType)1)/REG_EPS : exp(eta[i]));
			res[i] = tmp/(1+tmp);
		}
		return res;

		
// 		Col<double> tmp = exp(eta);
// 		cout << "linkinv: tmp has nan before check: " << tmp.has_nan() << endl;
// 		tmp.elem(eta < -30.).fill(DBL_EPSILON);
// 		tmp.elem(eta > 30.).fill(((double)1)/DBL_EPSILON);
// 		cout << "linkinv: tmp has nan after check: " << tmp.has_nan() << endl;
// 		cout << "linkinv: tmp min: " << min(tmp) << " tmp max: " << max(tmp) << endl;
// 		Col<double> res=tmp/(1 + tmp);
// 		cout << "linkinv: res has nan: " << res.has_nan() << endl;
// 		return tmp/(1 + tmp);
		
// 		static R_INLINE double x_d_opx(double x) {return x/(1 + x);}		
		
// 		static const double THRESH = 30.;
// 		static const double MTHRESH = -30.;	
// 		static const double INVEPS = 1/DOUBLE_EPS;
		
// 		SEXP ans = PROTECT(shallow_duplicate(eta));
// 		int i, n = LENGTH(eta);
// 		double *rans = REAL(ans), *reta = REAL(eta);
// 
// 		if (!n || !isReal(eta))
// 		error(_("Argument %s must be a nonempty numeric vector"), "eta");
// 		for (i = 0; i < n; i++) {
// 		double etai = reta[i], tmp;
// 		tmp = (etai < MTHRESH) ? DOUBLE_EPS : ((etai > THRESH) ? INVEPS : exp(etai));
// 		rans[i] = x_d_opx(tmp);
// 		}
// 		UNPROTECT(1);
// 		return ans;
	}
	
	//   mu.eta: function: derivative of the inverse-link function with
	//           respect to the linear predictor.  If the inverse-link
	//           function is mu = ginv(eta) where eta is the value of the
	//           linear predictor, then this function returns
	//           d(ginv(eta))/d(eta) = d(mu)/d(eta).
	inline glmMatF mu_eta(const glmMatF & eta) const{
		glmColF tmp(eta.n_elem);
#pragma omp parallel for		
		for (int i = 0; i < eta.n_elem; i++) {
			glmFType opexp = 1 + exp(eta[i]);
			tmp[i] = (eta[i] > 30. || eta[i] < -30.) ? REG_EPS : exp(eta[i])/(opexp * opexp);
		}
		return tmp;
// 		SEXP ans = PROTECT(shallow_duplicate(eta));
// 		int i, n = LENGTH(eta);
// 		double *rans = REAL(ans), *reta = REAL(eta);
// 
// 		if (!n || !isReal(eta))	error(_("Argument %s must be a nonempty numeric vector"), "eta");
// 		for (i = 0; i < n; i++) {
// 		double etai = reta[i];
// 		double opexp = 1 + exp(etai);
// 
// 		rans[i] = (etai > THRESH || etai < MTHRESH) ? DOUBLE_EPS :
// 			exp(etai)/(opexp * opexp);
// 		}
// 		UNPROTECT(1);
// 		return ans;
	}

};
