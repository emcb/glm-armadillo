#pragma once
#define ARMA_DONT_PRINT_ERRORS
#define ARMA_DONT_USE_HDF5
#define ARMA_DONT_USE_WRAPPER
#define ARMA_NO_DEBUG
#include<armadillo>
#include <limits>

typedef double glmFType;
typedef int glmIType;
typedef unsigned int glmUIType;
typedef arma::uword glmAType;

typedef arma::Mat<glmFType> glmMatF;
typedef arma::Mat<glmIType> glmMatI;
typedef arma::Col<glmFType> glmColF;
typedef arma::Col<glmIType> glmColI;
typedef arma::Col<glmAType> glmColA;

static const glmFType REG_EPS= std::numeric_limits<glmFType>::epsilon();

class glmException:public std::exception{
public:
	std::string msg;
	glmException(const std::string & what):std::exception(),msg(what){
	}
	virtual const char* what() const throw(){
		return msg.c_str();
	}
};


class glmControl{
public:
	glmIType i_maxit;
	glmFType i_glmTol;
	glmFType i_lmTol;
	bool i_check;
	bool i_trace;
	glmControl(glmIType maxit=25,glmFType glmTol=1e-8, glmFType lmTol=1e-8/1000,bool check=false,bool trace=false):i_maxit(maxit),i_glmTol(glmTol),i_lmTol(lmTol),i_check(check),i_trace(trace){
	}
};

