#pragma once
#include "links/identityLink.h"
#include "links/logLink.h"
#include "links/logitLink.h"


#include "families/gaussianFamily.h"
#include "families/binomialFamily.h"
#include "families/poissonFamily.h"
#include "families/negativeBinomialFamily.h"

#ifdef GLM_USE_LAPACK_SOLVER
#include "solvers/linearSolver_lapack.h"
#else
#include "solvers/linearSolver_eigen.h"
#endif

#include "solvers/glmSolver.h"
#include "solvers/glmSolverNB.h"




