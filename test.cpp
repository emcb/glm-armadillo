//#define GLM_USE_LAPACK_SOLVER
#include "glm-armadillo/glm.h"

#include <chrono>

using namespace std;
using namespace arma;

/*
int test_speed(int argc, char ** argv){
	bool nm=false;
	int nrep1=atoi(argv[1]);
	int nrep2=atoi(argv[2]);
	
	if(argc>3){
		nm=true;
	}

	mat X,Xf;
	vec y;
	if(nm){
		X=randu<mat>(18041,2);
		X.save("example_X.mat",csv_ascii);
		y=floor(abs(randn<Col<double>>(X.n_rows)) * 100)+1;
		y.save("example_y.mat",csv_ascii);
	}else{
		X.load("example_X.mat",csv_ascii);
		y.load("example_y.mat",csv_ascii);
	}
	Xf = join_rows( mat(X.n_rows,1,fill::ones),X);
	//Xf = join_rows( mat(X.n_rows,1,fill::ones),join_rows( mat(X.n_rows,1,fill::ones),X));
	cout << "X [ " << Xf.n_rows << " x " << Xf.n_cols << " ]" << endl;
	cout << "y [ " << y.n_rows << " x " << y.n_cols << " ]" << endl;	
	
	chrono::duration<double> dur;
	auto first_time = std::chrono::high_resolution_clock::now();
	Col<double> times(nrep1);
	Col<double> itimes(nrep1*nrep2);
	for(int r=0;r<nrep1;r++){
		int b=nrep2*r;
		auto prime_time = std::chrono::high_resolution_clock::now();		
#pragma omp parallel for
		for(int i=0;i<nrep2;i++){
			auto t0 = std::chrono::high_resolution_clock::now();
			glm_fit<linearSolver,gaussianFamily<identityLink> > GLM(Xf,y,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true);
			//glm_fit<poissonFamily<glmLinkLog>> GLM(Xf,y,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),poissonFamily<glmLinkLog>(glmLinkLog()),glmControl(),true);
			//glm_fit_nb GLM(Xf,y,Col<double>(),Col<double>(),Col<double>(), Col<double>(),Col<double>(),glmControl(),true,true,-1);
			Mat<double> table=GLM.coeffTable();			
 			chrono::duration<double> d = std::chrono::high_resolution_clock::now() - t0;
			itimes[b+i]=d.count();
			
		}
		dur = std::chrono::high_resolution_clock::now() - prime_time;
		times[r]=dur.count();
		//cerr << "Test " << r+1 << " Time elapsed: " << times[r] << endl;
	}
	dur = std::chrono::high_resolution_clock::now() - first_time;
	
	cout << "External loop (" << nrep1 << " samples)" << endl;
	cout << "    Avg: " << mean(times) << endl;
	cout << "     Sd: " << stddev(times) << endl;
	cout << "Internal loop (" << nrep1*nrep2 << " samples)" << endl;
	cout << "    Avg: " << mean(itimes) << endl;
	cout << "     Sd: " << stddev(itimes) << endl;
	cout << "Overall: " << dur.count() << endl;
	return 0;	
}
*/

int test_results(int argc, char ** argv){
	bool nm=false;
	
	if(argc>1){
		nm=true;
	}

	Mat<double> X,Xf;
	Col<double> y;

	X=randn<Mat<double>>(18041,2);
	y=floor(abs(randn<Col<double>>(X.n_rows)) * 100)+1;
	//adding intecept
	Xf = join_rows( mat(X.n_rows,1,fill::ones),X);
	//adding intecept and a second identical column to test rank revealing capabilities
	//Xf = join_rows( mat(X.n_rows,1,fill::ones),join_rows( mat(X.n_rows,1,fill::ones),X));
	cout << "X [ " << Xf.n_rows << " x " << Xf.n_cols << " ]" << endl;
	cout << "y [ " << y.n_rows << " x " << y.n_cols << " ]" << endl;

	{		cout << "------------------------>GAUSSIAN IDENTITY" << endl;
		auto prime_time = std::chrono::high_resolution_clock::now();
		glm_fit<linearSolver,gaussianFamily<identityLink>> GLM(Xf,y,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true);
		
		Mat<double> table=GLM.coeffTable();
		chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - prime_time;
		cerr << "Elapsed time: " << dur.count() << endl;		
		GLM.summary();
		cerr << table << endl;
	}
	
	{		cout << "------------------------>GAUSSIAN LOG" << endl;
		auto prime_time = std::chrono::high_resolution_clock::now();
		Col<double> yb=y+1;
		glm_fit<linearSolver,gaussianFamily<logLink>> GLM(Xf,yb,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true);

		Mat<double> table=GLM.coeffTable();
		chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - prime_time;
		cerr << "Elapsed time: " << dur.count() << endl;
		GLM.summary();		
		cerr << table << endl;
	}
	
	{		cout << "------------------------>POISSON LOG" << endl;
		auto prime_time = std::chrono::high_resolution_clock::now();
		glm_fit<linearSolver,poissonFamily<logLink>> GLM(Xf,y,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true);
		Mat<double> table=GLM.coeffTable();				
		chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - prime_time;
		cerr << "Elapsed time: " << dur.count() << endl;
		GLM.summary();
		cerr << table << endl;
	}

	{		cout << "------------------------>BINOMIAL LOGIT (LOGISTIC)" << endl;
		auto prime_time = std::chrono::high_resolution_clock::now();
		Col<double> ty=conv_to< Col<double> >::from(y>0);
		glm_fit<linearSolver,binomialFamily<logitLink>> GLM(Xf,ty,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true);
		Mat<double> table=GLM.coeffTable();				
		chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - prime_time;
		cerr << "Elapsed time: " << dur.count() << endl;
		GLM.summary();		
		cerr << table << endl;
	}
	
	{		cout << "------------------------>NEGATIVE BINOMIAL LOG" << endl;
		auto prime_time = std::chrono::high_resolution_clock::now();
	
		glm_fit_nb<linearSolver,logLink> GLM(Xf,y,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true,true,-1);

		Mat<double> table=GLM.coeffTable();				
		chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - prime_time;
		cerr << "Elapsed time: " << dur.count() << endl;
		GLM.summary();		
		cerr << table << endl;
	}

	{		cout << "------------------------>NEGATIVE BINOMIAL LOG START THETA" << endl;
		auto prime_time = std::chrono::high_resolution_clock::now();
	
		glm_fit_nb<linearSolver,logLink> GLM(Xf,y,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true,true,1.407513);

		Mat<double> table=GLM.coeffTable();				
		chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - prime_time;
		cerr << "Elapsed time: " << dur.count() << endl;
		GLM.summary();		
		cerr << table << endl;
	}

	{		cout << "------------------------>NEGATIVE BINOMIAL LOG KNOWN theta" << endl;
		auto prime_time = std::chrono::high_resolution_clock::now();
		glm_fit<linearSolver,negativeBinomialFamily<logLink>> GLM(y.n_rows,Xf.n_cols);
		GLM.i_family.i_theta=1.407513;
		GLM.solve(Xf,y,Col<double>(),Col<double>(),Col<double>(),Col<double>(),Col<double>(),glmControl(),true,true);
		Mat<double> table=GLM.coeffTable();				
		chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - prime_time;
		cerr << "Elapsed time: " << dur.count() << endl;
		GLM.summary();		
		cerr << table << endl;
	}
	
	return 0;	
}

int main(int argc, char ** argv){
	int ret=-1;
	try{
		ret=test_results(argc,argv);
	}catch(exception &e){
		cout << e.what() << endl;
	}
	return ret;
}
